#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

echo "Logging in to 1password"
eval "$(op signin gitlab.1password.com)"

read -rp "Customer Name (lowercase chars for email @closedbeta.com): " CUSTOMER_NAME
read -rp "Issue Link: " ISSUE_URL
read -rp "macOS Version (eg: 11): " MACOS_VERSION
read -rp "Xcode Version (eg: 12.4): " XCODE_VERSION
read -rp "Customer runner registration token: " GITLAB_REGISTRATION_TOKEN

echo "The latest base image to use should be shown below. If you want to tag a new version from master, run:"
echo ""
cat << EOF
  ORKA_API_URL="\$(jq -r '."api-url"' < ~/.config/configstore/orka-cli.json)" \\
  ORKA_API_TOKEN="\$(jq -r ".token" < ~/.config/configstore/orka-cli.json)" \\
  TIMEOUT=1800 ./scripts/ci/orka POST resources/image/copy \\
    image="runner-$MACOS_VERSION-$XCODE_VERSION.img" \\
    new_name="runner-$XCODE_VERSION-\$(date -u '+%Y%m%d').img"
EOF
echo ""

echo ""
orka image list | grep "runner-$XCODE_VERSION-202" || echo "No image has been tagged yet"
echo ""

read -rp "Base Image: " BASE_IMAGE_NAME

ORKA_USER="$CUSTOMER_NAME@closedbeta.com"

echo "Creating 1password entry"
op create item login --title "Orka closed beta user - $CUSTOMER_NAME" --url "$ISSUE_URL" --vault Verify --account gitlab --generate-password "username=$ORKA_USER"
ORKA_PASSWORD=$(op get item --vault Verify --account gitlab --fields password "Orka closed beta user - $CUSTOMER_NAME")

echo "Creating orka user"
LICENSE_KEY=$(op get item --vault Verify --account gitlab --fields password "Orka License")
orka user create -y -e "$ORKA_USER" --password "$ORKA_PASSWORD" -l "$LICENSE_KEY"
orka user group --email "$ORKA_USER" --group closedbeta -y

echo "Creating the vm"
docker build -t macos-runner-image-builder -f ./dockerfiles/ci/Dockerfile ./dockerfiles/ci
docker run --rm -it \
  -e ANSIBLE_HOST_KEY_CHECKING=False \
  -e ANSIBLE_USER_UPDATED_PASSWORD="$(op get item --vault Verify --account gitlab --fields password "orka base VM")" \
  -e BASE_IMAGE_NAME="$BASE_IMAGE_NAME" \
  -e CUSTOMER_NAME="$CUSTOMER_NAME" \
  -e GITLAB_REGISTRATION_TOKEN="$GITLAB_REGISTRATION_TOKEN" \
  -e ORKA_API_URL="$(op get item --vault Verify --account gitlab --fields "API URL" "Orka VPN")" \
  -e ORKA_LICENSE_KEY="$LICENSE_KEY" \
  -e ORKA_NO_VPN="true" \
  -e ORKA_PASSWORD="$ORKA_PASSWORD" \
  -e ORKA_USER="$ORKA_USER" \
  -e MACOS_VERSION="$MACOS_VERSION" \
  -e XCODE_VERSION="$XCODE_VERSION" \
  -v "$(pwd)":/build \
  -w /build \
  macos-runner-image-builder \
  scripts/closed_beta/create_env

echo "Reply to the user in $ISSUE_URL with the following:"
echo "======"
RUNNER_VERSION=$(grep gitlab_runner_version: group_vars/all/gitlab_runner.yml | cut -d " " -f2)
cat << EOF
Hi USER

Your beta macOS runner is online now! You should be able to execute builds,
with Xcode $XCODE_VERSION and Runner $RUNNER_VERSION on macOS $MACOS_VERSION (runner tags: \`macos,macos-$MACOS_VERSION,shell,shared-macos,xcode-$XCODE_VERSION\`).

Please remember to reset your runner registration token for security reasons.

Let us know if you have any questions! I'll leave this issue open for future discussions.

/unassign ME
EOF
echo "====="
