#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

# Sometimes when ansible steps take a long time and don't produce any output
# while they run (such as compiling a dependency from source, or extracting an Xcode archive)
# the VPN will die because of Idle Timeout:
#      Received server disconnect: b0 'Idle Timeout'
#      Send BYE packet: Server request
#      Session terminated by server; exiting.
#
# By pinging the Orka API endpoint every minute we ensure we always have traffic going through the VPN
# Note: We use HTTP because it doesn't reply to ping.

while true ; do
  curl -s --show-error "$ORKA_API_URL" > /dev/null || true
  sleep 60
done
