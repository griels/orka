#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

scripts/keep-alive.sh &

if [[ -z ${BASE_IMAGE_NAME:-} ]]; then
  # For base builds, this is set in the YAML to the starter OS image
  # For following stages, this can be:
  # - set by the `.base-image.env` artifact saved in the previous stage,
  #   for example `toolchain` will use the image built previously
  #   in the same pipeline by the `base` stage
  # - unset, in that case the previous stage was skipped by change detection
  #   and we should start off the master image for that previous stage
  export BASE_IMAGE_NAME="$PREVIOUS_IMAGE_TYPE.img"
fi

OUTPUT_IMAGE_NAME="$IMAGE_TYPE.img"
if [[ -z ${CI_COMMIT_BRANCH:-} || "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" ]]; then
  # for MRs, prefix the image name with the MR ID
  OUTPUT_IMAGE_NAME="mr-$CI_MERGE_REQUEST_IID-$OUTPUT_IMAGE_NAME"
fi

# replace TIMESTAMP in the output image name by the time of the pipeline start, with minute precision
# use pipeline start rather than job start so multiple images built from the same pipeline have the same version
TIMESTAMP=$(date --date "$CI_PIPELINE_CREATED_AT" "+%Y%m%d%H%M")
OUTPUT_IMAGE_NAME="${OUTPUT_IMAGE_NAME/TIMESTAMP/$TIMESTAMP}"

if [ ${#OUTPUT_IMAGE_NAME} -gt 29 ]; then
  echo "Error: \$OUTPUT_IMAGE_NAME $OUTPUT_IMAGE_NAME is longer than 29 chars."
  exit 1
fi

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp)

make orka-create-vm TMPFILE="$TMPFILE"
VM_ID="$(jq -r .vm_id $TMPFILE)"
echo "Created VM with ID '$VM_ID'"

echo "Running make $MAKE_TARGET with OUTPUT_IMAGE_NAME $OUTPUT_IMAGE_NAME"
make setup
make "$MAKE_TARGET" VM_ID="$VM_ID"
make orka-save-vm-image VM_ID="$VM_ID" OUTPUT_IMAGE_NAME="$OUTPUT_IMAGE_NAME"

echo "BASE_IMAGE_NAME=$OUTPUT_IMAGE_NAME" > .base-image.env
